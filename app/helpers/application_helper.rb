# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  def verificar_autorizacion
    usuario = Usuario.find(session[:usuario_id])
    unless tiene_derecho?(usuario)
      flash[:notice] = "No está autorizado para realizar esta acción"
      request.env["HTTP_REFERER"] ? (redirect_to :back) : (redirect_to :controller => 'login', :action => 'bienvenida')
      return false
    end
  end

  def tiene_derecho?(usuario)
    usuario.tiene_derecho_para?(params[:action], params[:controller])
  end

  def link_to_if_authorized(enlace, params) 
    link_to(enlace, params) if Usuario.find(session[:usuario_id]).tiene_derecho_para?(params[:action].to_s, params[:controller].to_s)
  end
  
  def nombre_de_usuario
    Usuario.find(session[:usuario_id]).nombre
  end

  def dinero(num)
    number_to_currency(num, :precision => 0, :unit => "$", :separator => ",", :delimiter => ".")
  end
end
