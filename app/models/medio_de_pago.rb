class MedioDePago < ActiveRecord::Base
  has_many :mandatos
  belongs_to :persona
end
