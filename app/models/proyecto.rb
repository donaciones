class Proyecto < ActiveRecord::Base
  has_and_belongs_to_many :mandatos

  validates_presence_of :nombre
end
