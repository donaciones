class Derecho < ActiveRecord::Base
  has_and_belongs_to_many :roles, :class_name => "Rol", :join_table => "derechos_roles"

  def self.actualizar_derechos
    controllers=Dir[RAILS_ROOT + "/app/controllers/**/*_controller.rb"].each {|f| require f}

    controllers = controllers.collect{|f| Kernel.const_get(f.gsub(/.*\/(.*).rb$/,'\1').camelize)}
    controllers.each do |controller|
      controller_nombre = controller.to_s.underscore.gsub(/_[^_]*$/, '')
      actions = controller.action_methods
      actions = actions - ApplicationController.private_instance_methods
      actions.delete("wsdl")
      actions=actions.to_a

      actions.each do |action|
        unless self.find_by_action_and_controller(action, controller_nombre)
          self.create(:action => action, :controller => controller_nombre)
        end
      end

    end

  end

  def tiene_derecho_para?(nombre_action, nombre_controller)
    action == nombre_action and controller == nombre_controller
  end
end
