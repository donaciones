class Banco < ActiveRecord::Base
  has_many :cuentas_corrientes

  validates_presence_of :codigo, :nombre
  validates_inclusion_of :alineacion_output, :in => ["derecha", "izquierda"]
end
