class Persona < ActiveRecord::Base
  belongs_to :comuna
  has_many :medios_de_pago, :class_name => "MedioDePago"
  has_many :cuentas_corrientes, :class_name => "CuentaCorriente"
  has_many :tarjetas_de_credito, :class_name => "TarjetaDeCredito"
  has_many :mandatos, :through => :medios_de_pago

  before_validation :formatear_rut

  validates_presence_of :nombres,
    :apellido_paterno,
    :apellido_materno,
    :rut

  validates_inclusion_of :sexo, :in => ["masculino", "femenino" ]

  #  validates_email_format_of :email
  
  validates_uniqueness_of :rut

  def validate
    errors.add(:rut, "no es correcto") unless rut_correcto

  end

  def self.apellido_empieza_con(inicio)
    self.find(:all, :conditions => ["apellido_paterno like concat(?,'%')", inicio], :order => 'apellido_paterno, apellido_materno')
  end

  def self.find_mixed(query)
    find(:all, :conditions => ["concat(nombres, ' ', apellido_paterno, ' ', apellido_materno, ' ', rut) like concat('%', ?, '%')", query], :order => 'apellido_paterno, apellido_materno')    
  end



  def formatear_rut
    self.rut = self.rut.gsub('.',"")
  end

  def self.normalizar_nombres
    self.find(:all).each do |p|
      p.nombres = p.nombres.titleize
      p.apellido_paterno = p.apellido_paterno.titleize
      p.apellido_materno = p.apellido_materno.titleize
      p.direccion1 = p.direccion1.titleize
      p.direccion2 = p.direccion2.titleize
      p.save
    end
  end

  def rut_correcto
    rut_test, dv = rut.to_s.upcase.gsub(/[^0-9K-]/,'').split("-")
    rut_test = rut_test.to_i
    dv = dv.to_s
    
    v=1
    s=0
    for i in (2..9)
      if i == 8
        v=2
      else
        v+=1
      end
      s+=v*(rut_test%10)
      rut_test/=10
    end
    
    s = 11 - s%11
    
    if s == 11
      correct_dv = "0"
    elsif s == 10
      correct_dv = "K"
    else
      correct_dv = s.to_s
    end

    dv == correct_dv
  end
  
  def nombre_completo
    [self.apellido_paterno, self.apellido_materno, self.nombres].join(" ")
  end
     
  def donaciones
    self.mandatos.collect{|m| m.donaciones}.flatten
  end

end
