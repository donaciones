class CuentaCorriente < MedioDePago
  belongs_to :banco
  validates_presence_of :numero
end
