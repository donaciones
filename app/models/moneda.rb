class Moneda < ActiveRecord::Base
  has_many :tipos_de_cambio, :class_name => "TipoDeCambio"
  has_many :mandatos, :class_name => "Mandato"

  validates_presence_of :nombre, :simbolo
  
  def self.find_primaria
    self.find_by_es_primaria(true)
  end
  
  def self.find_no_primarias
    self.find_all_by_es_primaria(false)
  end

end
