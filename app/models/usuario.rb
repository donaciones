require 'digest/sha2'

class Usuario < ActiveRecord::Base

  has_and_belongs_to_many :roles, :class_name => "Rol", :join_table => 'roles_usuarios'

  validates_presence_of :nombre, :password_encriptado
  validates_confirmation_of :password
  validates_uniqueness_of :nombre

  def validate
    errors.add(:password, "no puede estar en blanco") if password.blank?
  end

  def password=(p)
    @password = p
    self.password_encriptado = Usuario.encriptar(p)
  end

  def self.autorizar(params)
    params = {"nombre" => "", "password" => ""}.merge(params)
    pwd_enc = self.encriptar(params["password"])
    u = Usuario.find_by_nombre_and_password_encriptado(params["nombre"], pwd_enc)
  end

  def cambiar_password(params)
    params = {'anterior' => '', 'nuevo' => '', 'nuevo_confirmation' => ''}.merge(params)
    if password_encriptado == Usuario.encriptar(params['anterior']) and params['nuevo'] == params['nuevo_confirmation']
      self.password = params['nuevo']
      save
    else
      nil
    end
  end
    
  def password
    @password
  end

  def self.encriptar(p)
    Digest::SHA256.hexdigest(p)
  end

  def tiene_derecho_para?(nombre_action, nombre_controller)
    return true if self.admin?
    roles.detect do |rol|
      rol.tiene_derecho_para?(nombre_action, nombre_controller)
    end
  end

  def ganar_roles(roles_ids)
    self.roles << Rol.find(roles_ids)
  end

  def perder_roles(roles_ids)
    self.roles = self.roles - Rol.find(roles_ids)
  end
  
  def admin?
    self.roles.find_by_nombre("Administrador")
  end

end
