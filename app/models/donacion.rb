class Donacion < ActiveRecord::Base
  belongs_to :mandato
  belongs_to :proceso
  belongs_to :respuesta

  def efectuada
    respuesta.nil? or respuesta.efectuada
  end

  def monto_efectivo
    return 0 if respuesta.nil? or !respuesta.efectuada
    return monto_real
  end

end
