require 'openssl'
require 'yaml'

class TarjetaDeCredito < MedioDePago
  belongs_to :tipo

  validates_presence_of :tipo, :fecha_de_vencimiento, :numero

  def validate
    errors.add(:numero, "tiene caracteres incorrectos") unless numero_con_caracteres_adecuados
    errors.add(:numero, "no sigue el formato del tipo de tarjeta") unless formato_correcto
  end


  def numero_con_caracteres_adecuados
    self.numero == nil or /^[0-9-]*$/ =~ self.numero
  end

  def formato_correcto
    mascara = self.tipo.mascara
    mascara.gsub!(/^/,'^')
    mascara.gsub!(/$/,'$')
    mascara.gsub!('X','\d')
    re = Regexp.new(mascara)
    re =~ self.numero
  end
  
  def before_save
    self.ultimos_digitos = self.numero.gsub(/.*(-\d+)$/, '\1')
    cp = OpenSSL::PKey::RSA.new(ClavePublica.contenido)
    self.numero_encriptado = cp.public_encrypt(self.numero).to_yaml
    self.numero = nil
  end

  def numero_enmascarado
    self.tipo.mascara.gsub(/(.*)-X+$/, '\1')+self.ultimos_digitos
  end

  def numero_desencriptar(clave_privada)
    clave_privada.private_decrypt(YAML.load(self.numero_encriptado))
  end

  def self.reencriptar(pk_vieja)

    pk_nueva = OpenSSL::PKey::RSA.generate(2048)

    self.transaction do
      ClavePublica.fijar(pk_nueva.public_key.to_s)
      self.find(:all).each do |t|
        t.numero = pk_vieja.private_decrypt(YAML.load(t.numero_encriptado))
        t.save!
      end

    end
    pk_nueva

  end

  def self.generar_ultimos
    self.find(:all).each do |t|
      cant_ultimos = t.tipo.mascara.gsub(/.*-(X+)$/,'\1').size
      t.ultimos_digitos = "-"+ t.numero[-cant_ultimos..-1]
      i=0
      s=""
      t.tipo.mascara.scan(/X+/) do |m|
        ped = t.numero[i..m.size+i-1]+"-"
        s << ped
        i+=m.size
      end
      t.numero=s[0..-2]
      t.save                                  # ahora reclama por encriptar en before save; hay que poner una clave_publica
    end
  end


  #def set_numero(num,clave_publica)
  #  self.numero_encriptado = clave_publica.public_encrypt(num)
  #end
end

