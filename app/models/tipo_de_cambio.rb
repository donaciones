class TipoDeCambio < ActiveRecord::Base
  belongs_to :moneda

  def self.asignar(moneda, fecha, valor)
    tc = self.find_by_fecha_and_moneda_id(fecha, moneda) || self.new
    tc.moneda = moneda
    tc.valor = valor
    tc.fecha = fecha
    tc.save
  end

  def self.asignar_primaria(fecha)
    m = Moneda.find_primaria
    self.asignar(m, fecha, 1)
  end
end
