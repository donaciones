class ClavePublica < ActiveRecord::Base

  def self.fijar(cont)
    clave = self.find(1)
    clave.contenido = cont
    clave.save!
  end

  def self.contenido
    clave = self.find(1)
    clave.contenido
  end

end
