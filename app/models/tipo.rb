class Tipo < ActiveRecord::Base
  has_many :tarjetas_de_credito

  validates_presence_of :nombre, :mascara
  validates_format_of :mascara, :with => /^X[X-]*X$/i
end
