class Proceso < ActiveRecord::Base
  has_many :donaciones, :dependent => :destroy
  has_many :donaciones_huerfanas, :dependent => :destroy

  validates_presence_of :fecha, :periodo, :tipo

  def nombre_periodo
    self.periodo.strftime("%b-%Y")
  end


  def asociar_mandatos_como_donaciones(mandatos)
    mandatos.each do |mandato|
      donacion = Donacion.new
      donacion.mandato = mandato
      multiplicador = mandato.moneda.tipos_de_cambio.find_by_fecha(self.fecha).valor
      donacion.monto_real = (mandato.monto * multiplicador).round # Multiplicar por el tipo de cambio correspondiente 
      donacion.save
      self.donaciones << donacion
    end
  end
  
  def numero_de_registros
    self.donaciones.size
  end
  
  def numero_de_registros_efectivos
    self.donaciones.select{|d| d.efectuada}.size
  end
  
  def monto_total
    self.donaciones.sum(:monto_real)
  end

  def monto_total_efectivo
    self.donaciones.inject(0) {|suma, d| suma+d.monto_efectivo}
  end

# Efectúa una comparación entre las donaciones indicadas en la BDU y las que 
# aparecen en el sistema de donaciones.
# - Anteriormente, si una donación estaba en el sistema, pero no en la BDU, se
# eliminaba, ahora se conserva, a la espera de lo que se responda a través de 
# rendiciones.
# - Ahora, además, se agregan las donaciones en la BDU que no están en el sistema,
# como donaciones huérfanas
  def filtrar_con_bdu(mandatos_bdu)


############################################
# Éste es el código que eliminaba donaciones que no estaban en la BDU
#    self.donaciones.each do |donacion|
#      unless donacion.mandato.nil?
#        unless mandatos_bdu.find {|rut, banco| rut == RUT.reducir(donacion.mandato.medio_de_pago.persona.rut) and banco == donacion.mandato.medio_de_pago.banco.codigo}
#          donacion.destroy
#        end
#      end
#    end
############################################

    # eliminar del listado de mandatos BDU aquellos que están en donaciones
    self.donaciones.each do |donacion|
      unless donacion.mandato.nil?
        mandatos_bdu.delete(mandatos_bdu.find {|rut, banco| rut == RUT.reducir(donacion.mandato.medio_de_pago.persona.rut) and banco == donacion.mandato.medio_de_pago.banco.codigo})
      end
    end
    
    # crear donaciones huérfanas para los mandatos BDU que no están en donaciones
    mandatos_bdu.each do |rut, cod_banco|
      d = DonacionHuerfana.new(:rut => rut)
      d.banco = Banco.find_by_codigo(cod_banco)
      d.proceso = self
      d.save
    end
   
    self.estado = "iniciado"
    self.save
  end

  def filtrar_con_cargos(cargos)
    if self.tipo == 'pat'
      filtrar_con_cargos_pat(cargos)
    elsif self.tipo == 'pac'
      filtrar_con_cargos_pac(cargos)
    end
  end

  def filtrar_con_cargos_pac(cargos)
    self.donaciones.each do |donacion|
      unless donacion.mandato.nil?
        if cargo = cargos.find {|cargo| cargo[:rut] == RUT.reducir(donacion.mandato.medio_de_pago.persona.rut) and cargo[:codigo_banco] == donacion.mandato.medio_de_pago.banco.codigo}
          donacion.monto_real = cargo[:monto]
          donacion.respuesta = RespuestaPac.find_by_codigo(cargo[:codigo_respuesta])
          donacion.save
        end
      end
    end

    self.estado = "finalizado"
    self.save
  end

  def filtrar_con_cargos_pat(cargos) #TODO
    self.donaciones.each do |donacion|
      unless donacion.mandato.nil?
        if cargo = cargos.find {|cargo| cargo[:rut] == RUT.reducir_sin_dv(donacion.mandato.medio_de_pago.persona.rut)} #TODO encriptar numtar para comparar con BD
          donacion.monto_real = cargo[:monto]
          donacion.respuesta = RespuestaPat.find_by_codigo(cargo[:codigo_respuesta])
          donacion.save
        end
      end
    end

    self.estado = "finalizado"
    self.save
  end

  def donaciones_array
    arreglo = []
    self.donaciones.each do |donacion|
      unless donacion.mandato.nil?
        arreglo << {
          :rut => donacion.mandato.medio_de_pago.persona.rut,
          :banco => donacion.mandato.medio_de_pago.banco.codigo,
          :apellido_paterno => donacion.mandato.medio_de_pago.persona.apellido_paterno,
          :apellido_materno => donacion.mandato.medio_de_pago.persona.apellido_materno,
          :nombres => donacion.mandato.medio_de_pago.persona.nombres,
          :monto => donacion.monto_real,
          :alineacion => donacion.mandato.medio_de_pago.banco.alineacion_output
        }
      end
    end
    arreglo
  end

  
  def donaciones_huerfanas
    DonacionHuerfana.find_all_by_proceso_id(self.id)
  end
  
end
