class Rol < ActiveRecord::Base
  set_table_name 'roles'
  has_and_belongs_to_many :usuarios, :join_table => 'roles_usuarios'
  has_and_belongs_to_many :derechos, :join_table => 'derechos_roles'

  validates_uniqueness_of :nombre

  def tiene_derecho_para?(nombre_action, nombre_controller)
    derechos.detect do |derecho|
      derecho.tiene_derecho_para?(nombre_action, nombre_controller)
    end
  end


  def ganar_derechos(derechos_ids)
    self.derechos << Derecho.find(derechos_ids)
  end

  def perder_derechos(derechos_ids)
    self.derechos = self.derechos - Derecho.find(derechos_ids)
  end

end
