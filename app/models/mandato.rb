class Mandato < ActiveRecord::Base
  belongs_to :moneda
  belongs_to :medio_de_pago
  has_and_belongs_to_many :proyectos
  has_many :donaciones

  validates_presence_of :monto,
                        :fecha_de_creacion,
                        :periodo_inicial,
                        :moneda

  validates_numericality_of :monto

  def self.find_vigentes_pac(fecha)

    self.find_by_sql(["select mandatos.* from mandatos, medios_de_pago where periodo_inicial <= ? and (indefinido=1 or ? <= periodo_final) and medios_de_pago.type='CuentaCorriente' and mandatos.habilitado = 1 and medio_de_pago_id = medios_de_pago.id",
                     fecha, fecha])

  end

  def self.find_vigentes_pat(fecha)
    periodo=fecha-fecha.day+1

    self.find_by_sql(["select mandatos.* from mandatos, medios_de_pago where periodo_inicial <= ? and (indefinido=1 or ? <= periodo_final) and medios_de_pago.type='TarjetaDeCredito' and mandatos.habilitado = 1 and fecha_de_vencimiento >= ? and medio_de_pago_id = medios_de_pago.id",
                     fecha, fecha, periodo])

  end

  def self.find_vigentes(fecha, tipo)
    case tipo
    when 'pac'
      self.find_vigentes_pac(fecha)
    when 'pat'
      self.find_vigentes_pat(fecha)
    end
  end

end
