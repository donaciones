class ComunasController < ApplicationController
  def listar
    @comunas = Comuna.find(:all)
  end

  def agregar
    @comuna = Comuna.new(params[:comuna])
    if request.post?
      if @comuna.save
        redirect_to :action => :listar
      end
    end
  end

  def editar
    @comuna = Comuna.find(params[:id])
    if request.post?
      if @comuna.update_attributes(params[:comuna])
        redirect_to :action => :listar
      end
    end
  end

  def eliminar
    @comuna = Comuna.find(params[:id])
    @comuna.destroy
    redirect_to :action => :listar
  end


end
