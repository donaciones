require 'openssl'

class TarjetasController < ApplicationController
  def reencriptar_tarjetas
    if request.post?
      begin
        pk_vieja= OpenSSL::PKey::RSA.new(params[:clave_privada][0].read)

        pk_nueva = TarjetaDeCredito.reencriptar(pk_vieja)

        send_data(pk_nueva.to_s, :filename => "clave_privada_#{Time.now.to_date.to_s.delete('-')}.pem")

        flash[:notice] = "Las tarjetas han sido reencriptadas exitosamente"
      rescue
        flash[:notice] = "El archivo de clave privada no es correcto"
        redirect_to :action => 'iniciar_reencriptacion'
      end

    end
  end
end
