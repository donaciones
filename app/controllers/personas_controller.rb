class PersonasController < ApplicationController
  ############
  # PERSONAS #
  ############

  def listar
    params[:letra] ||= 'a'
    @personas = Persona.apellido_empieza_con(params[:letra])
  end

  def buscar
    @personas = Persona.find_mixed(params[:consulta][:nombre])
    session[:busqueda] = params[:consulta][:nombre]
    render :action => 'listar'
  end

  def agregar
    @persona = Persona.new(params[:persona])
    @comunas = Comuna.find(:all, :order => 'nombre')
    if request.post?
      if @persona.save
        redirect_to :action => :editar, :id => @persona
      end
    end
  end

  def editar
    @persona = Persona.find(params[:id])
    @comunas = Comuna.find(:all)
    @cuentas_corrientes = @persona.cuentas_corrientes
    @tarjetas = @persona.tarjetas_de_credito 
    @mandatos = @persona.mandatos
    @donaciones = @persona.donaciones
    if request.post?
      if @persona.update_attributes(params[:persona])
        flash[:notice] = 'Los datos se actualizaron correctamente'
      end
    end
  end

  def eliminar
    @persona = Persona.find(params[:id])
    @persona.destroy
    redirect_to :action => :listar
  end

  ######################
  # CUENTAS CORRIENTES #
  ######################

  def agregar_cuenta_corriente
    if request.get?
      @persona = Persona.find(params[:id])
      @cuenta_corriente = CuentaCorriente.new
      @bancos = Banco.find(:all)
    elsif request.post?
      params[:cuenta_corriente][:persona_id] = params[:id]
      @cuenta_corriente = CuentaCorriente.new(params[:cuenta_corriente])

      if @cuenta_corriente.save
        redirect_to :action => :editar, :id => params[:id]
      else
        @persona = Persona.find(params[:id])
        @bancos = Banco.find(:all)
      end
    end
  end

  def eliminar_cuenta_corriente
    @cuenta = CuentaCorriente.find(params[:id])
    @persona = @cuenta.persona
    @cuenta.destroy
    redirect_to :action => :editar, :id => @persona.id
  end

  #######################
  # TARJETAS DE CRÉDITO #
  #######################

  def agregar_tarjeta
    if request.get?
      @persona = Persona.find(params[:id])
      @tarjeta = TarjetaDeCredito.new
      @tipos = Tipo.find(:all).collect{|t| ["#{t.nombre} (#{t.mascara})", t.id]}
    elsif request.post?
      params[:tarjeta][:persona_id] = params[:id]
      @tarjeta = TarjetaDeCredito.new(params[:tarjeta])
      if @tarjeta.save
        redirect_to :action => :editar, :id => params[:id]
      else
        @persona = Persona.find(params[:id])
        @tipos = Tipo.find(:all).collect{|t| ["#{t.nombre} (#{t.mascara})", t.id]}
      end
    end
  end

  def eliminar_tarjeta
    @tarjeta = TarjetaDeCredito.find(params[:id])
    @persona = @tarjeta.persona
    @tarjeta.destroy
    redirect_to :action => :editar, :id=>@persona.id
  end

  ############
  # MANDATOS #
  ############

  def agregar_mandato
    if request.get?
      @persona = Persona.find(params[:id])
      @mandato = Mandato.new 
      @cuentas = @persona.cuentas_corrientes.find(:all).collect{|c| ["Cta. Cte. #{c.numero} #{c.banco.nombre}", c.id]}
      @tarjetas = @persona.tarjetas_de_credito.find(:all).collect{|t| ["Tarjeta #{t.tipo.nombre} #{t.numero_enmascarado} (#{t.fecha_de_vencimiento.strftime('%b-%Y')})", t.id]}
      @medios = @cuentas + @tarjetas
      @monedas = Moneda.find(:all).collect{|m| ["#{m.nombre} (#{m.simbolo})", m.id]}
      @proyectos = Proyecto.find(:all).collect{|p| [p.nombre, p.id]}
    elsif request.post?
      @mandato = Mandato.new(params[:mandato])
      @mandato.habilitado = true
      if params[:proyectos]
        params[:proyectos].each do |proy_id|
          @mandato.proyectos << Proyecto.find(proy_id) unless proy_id.blank? # TODO: la template pasa un id en blanco
        end
      end
      if @mandato.save
        redirect_to :action => :editar, :id =>  params[:id]
      else 
        @persona = Persona.find(params[:id])
        @mandato = Mandato.new 
        @cuentas = @persona.cuentas_corrientes.find(:all).collect{|c| ["Cta. Cte. #{c.numero} #{c.banco.nombre}", c.id]}
        @tarjetas = @persona.tarjetas_de_credito.find(:all).collect{|t| ["Tarjeta #{t.tipo.nombre} #{t.numero_enmascarado}", t.id]}
        @medios = @cuentas + @tarjetas
        @monedas = Moneda.find(:all).collect{|m| ["#{m.nombre} (#{m.simbolo})", m.id]}
        @proyectos = Proyecto.find(:all).collect{|p| [p.nombre, p.id]}
      end
    end
  end

  def inhabilitar_mandato
    @mandato = Mandato.find(params[:id])
    @mandato.habilitado = false
    @mandato.save
    persona = @mandato.medio_de_pago.persona
    redirect_to :action => :editar, :id => persona
  end

  def eliminar_mandato
    @mandato = Mandato.find(params[:id])
    @persona = @mandato.medio_de_pago.persona
    @mandato.destroy
    redirect_to :action => :editar, :id => @persona
  end

end
