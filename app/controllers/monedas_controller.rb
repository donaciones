class MonedasController < ApplicationController
  def listar
    @monedas = Moneda.find :all
  end

  def agregar
    @moneda = Moneda.new(params[:moneda])
    if request.post?
      if @moneda.save
        redirect_to :action => :listar
      end
    end
  end


  def editar
    @moneda = Moneda.find params[:id]
    if request.post?
      if @moneda.update_attributes(params[:moneda])
        redirect_to :action => :listar
      end
    end
  end

  def eliminar
    @moneda = Moneda.find(params[:id])
    @moneda.destroy
    redirect_to :action => :listar
  end

end
