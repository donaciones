require 'openssl'
require 'archivos_pat'
require 'archivos_pac'

class ProcesosController < ApplicationController

  def listar
    params[:criterio_orden] ||= 'id' 
    @procesos = Proceso.find(:all).sort_by{|x| x.tipo;x.method(params[:criterio_orden]).call}
  end

  def bajar_listado_procesos
    @procesos = Proceso.find(:all)
    texto_archivo = "id;Período;Tipo;Estado;Monto comprometido;Monto efectivo;Registros;Registros efectivos\n"
    @procesos.each do |proceso|
      texto_archivo += "#{proceso.id};#{proceso.periodo};#{proceso.tipo.upcase};#{proceso.estado.humanize};#{"%.0f"%proceso.monto_total};#{"%.0f"%proceso.monto_total_efectivo};#{proceso.numero_de_registros};#{proceso.numero_de_registros_efectivos}\n"
    end
    send_data(texto_archivo, :filename => "procesos_donaciones.csv")
  end

  def iniciar
    @proceso = Proceso.new(params[:proceso])
    if request.get?
      @monedas = Moneda.find_no_primarias
    elsif request.post?
      
      if @proceso.tipo == 'pac'
        @proceso.estado = 'por_iniciar'
      else
        @proceso.estado = 'iniciado'
      end

      if @proceso.save

        monedas = Moneda.find_no_primarias
        monedas.each do |moneda|
          TipoDeCambio.asignar(moneda, @proceso.fecha, params[:tipos_de_cambio][moneda.id.to_s])
        end

        TipoDeCambio.asignar_primaria(@proceso.fecha)

        mandatos = Mandato.find_vigentes(@proceso.periodo, @proceso.tipo)

        @proceso.asociar_mandatos_como_donaciones(mandatos)

        redirect_to :action => 'ver_detalles', :id=>@proceso
      end
    end

  end

  def ver_donaciones
    @proceso = Proceso.find(params[:id])
    @donaciones = @proceso.donaciones
    @donaciones_huerfanas = @proceso.donaciones_huerfanas
  end

  def ver_detalles
    @proceso = Proceso.find(params[:id])

    case @proceso.tipo
    when 'pac'
      case @proceso.estado
      when 'por_iniciar'
        render :action => 'ver_detalles_pac_sin_bdu'
      when 'iniciado'
        render :action => 'ver_detalles_pac_sin_cargos'
      when 'finalizado'
        render :action => 'ver_detalles_pac_con_cargos'
      end
    when 'pat'
      case @proceso.estado
      when 'iniciado'
        render :action => 'ver_detalles_pat_sin_cargos'
      when 'finalizado'
        render :action => 'ver_detalles_pat_con_cargos'
      end
    end
  end

  def bajar_archivo_pac
    proceso = Proceso.find(params[:id])
    texto_archivo = ArchivosPAC.generar_cargos(proceso.fecha, proceso.donaciones_array)
    send_data(texto_archivo, :filename => "felpac#{proceso.fecha.to_s.delete("-")}.txt")
  end

  def bajar_archivo_pat
    begin
      pk= OpenSSL::PKey::RSA.new(params[:archivo_clave_privada][0].read)
      
      texto_archivo = ""

      proceso = Proceso.find(params[:id])

      proceso.donaciones.each do |donacion|
        mandato = donacion.mandato
        session[:donid]=donacion.id
        tarjeta = mandato.medio_de_pago
        persona = tarjeta.persona
        rut_sin_guion = persona.rut.gsub(/-.$/, '')
        fecha_vencimiento = tarjeta.fecha_de_vencimiento.month.to_s+"/"+tarjeta.fecha_de_vencimiento.year.to_s.gsub(/..(..)/,'\1')
        nombre_con_formato = "#{persona.nombres} #{persona.apellido_paterno} #{persona.apellido_materno}"[0,40] 
        numero = tarjeta.numero_desencriptar(pk).delete("-")
        monto_real = "%.0f"%donacion.monto_real

        texto_archivo << "V;#{rut_sin_guion};#{numero};#{fecha_vencimiento};#{monto_real};#{nombre_con_formato};;;;#{persona.rut}\n"
      end

      send_data(texto_archivo, :filename => "felpat#{proceso.fecha.to_s.delete("-")}.txt")
    rescue OpenSSL::PKey::RSAError
      redirect_to :action => 'ver_detalles', :id => params[:id]
      flash[:notice] = "Clave privada incorrecta"
    end
  end

  def subir_bd_universo
    mandatos_bdu = ArchivosPAC.leer_bdu(params[:archivo_bdu][0])
    proceso = Proceso.find(params[:id])
    proceso.filtrar_con_bdu(mandatos_bdu)

    redirect_to :action => 'ver_detalles', :id => proceso

  end

  def subir_cargos
    proceso = Proceso.find(params[:id])

    cargos = 

    if proceso.tipo == 'pat'
      ArchivosPAT.leer_cargos(params[:archivo_cargos][0])
    elsif proceso.tipo == 'pac' 
      ArchivosPAC.leer_cargos(params[:archivo_cargos][0])
    end

    proceso.filtrar_con_cargos(cargos)
    redirect_to :action => 'ver_detalles', :id => proceso
    
  end
  
  def eliminar
    @proceso = Proceso.find(params[:id])
    @proceso.destroy
    redirect_to :action => :listar
  end
end
