class LoginController < ApplicationController

#  No verificar autenticación ni autorización, pues se trata de la pantalla
#  de login
  skip_before_filter :verificar_autenticacion, :verificar_autorizacion

  def index
    session[:usuario_id] = nil
  end

  def autorizar
    if u=Usuario.autorizar(params[:usuario])
      session[:usuario_id] = u.id
      redirect_to :action => 'bienvenida'
    else
      flash[:notice] = 'Usuario o password inválido'
      redirect_to :action => 'index'
    end
  end

  def salir
    session[:usuario_id] = nil
    redirect_to :action => 'index'
  end

  def bienvenida
    render :layout => 'application'
  end

end
