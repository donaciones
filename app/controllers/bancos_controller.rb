class BancosController < ApplicationController
  def listar
    @bancos = Banco.find(:all)
  end

  def agregar
    @banco = Banco.new(params[:banco])
    if request.post?
      if @banco.save
        redirect_to :action => :listar
      end
    end
  end

  def editar
    @banco = Banco.find params[:id]
    if request.post?
      if @banco.update_attributes(params[:banco])
        redirect_to :action => :listar
      end
    end
  end

  def eliminar
    @banco = Banco.find(params[:id])
    @banco.destroy
    redirect_to :action => :listar
  end

end
