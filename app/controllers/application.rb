# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  # Pick a unique cookie name to distinguish our session data from others'
  session :session_key => '_donaciones_session_id'

  before_filter :verificar_autenticacion,
  :verificar_autorizacion

  private

  # Filtro para verificar que existe una sesión activa
  def verificar_autenticacion # :doc:
    unless session[:usuario_id]
      redirect_to :controller => 'login'
      return false
    end
  end

  # Filtro para verificar los permisos que tiene el usuario activo
  def verificar_autorizacion
    usuario = Usuario.find(session[:usuario_id])
    unless tiene_derecho?(usuario)
      flash[:notice] = "No está autorizado para realizar esta acción"
      request.env["HTTP_REFERER"] ? (redirect_to :back) : (redirect_to :controller => 'login', :action => 'bienvenida')
      return false
    end
  end

  # Verifica que el usario tiene derecho para realizar
  # una acción en un controlador determinado
  def tiene_derecho?(usuario)
    usuario.tiene_derecho_para?(params[:action], params[:controller])
  end
  
end
