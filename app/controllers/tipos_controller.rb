class TiposController < ApplicationController
  def listar
    @tipos = Tipo.find :all
  end

  def agregar
    @tipo = Tipo.new(params[:tipo])
    if request.post?
      if @tipo.save
        redirect_to :action => :listar
      end
    end

  end

  def editar
    @tipo = Tipo.find params[:id]
    if request.post?
      if @tipo.update_attributes(params[:tipo])
        redirect_to :action => :listar
      end
    end
  end

  def eliminar
    @tipo = Tipo.find(params[:id])
    @tipo.destroy
    redirect_to :action => :listar
  end


end
