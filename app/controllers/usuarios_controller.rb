class UsuariosController < ApplicationController
  def listar
    @usuarios = Usuario.find(:all)
  end

  def agregar
    @usuario = Usuario.new(params[:usuario])
    if request.post?
      if @usuario.save
        flash[:notice] = 'Usuario agregado exitosamente'
        redirect_to :action => 'editar', :id => @usuario
      end
    end
  end

  def editar
    @usuario = Usuario.find(params[:id])
    if request.post?
      @usuario.ganar_roles(params[:roles_a_ganar]||[])
      @usuario.perder_roles(params[:roles_a_perder]||[])
      flash[:notice] = "Usuario modificado exitosamente"
    end
    @roles_que_tiene = @usuario.roles.collect { |r| [r.nombre, r.id]}
    @roles_que_no_tiene = (Rol.find(:all) - @usuario.roles).collect { |r| [r.nombre, r.id]}
  end

  def eliminar
    if session[:usuario_id].to_s == params[:id].to_s
      flash[:notice] = 'No se puede borrar a sí mismo'
    else
      Usuario.find(params[:id]).destroy
    end
    redirect_to :action => 'listar'
  end

end
