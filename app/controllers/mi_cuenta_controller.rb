class MiCuentaController < ApplicationController
  def cambiar_password
    if request.post?
      @usuario = Usuario.find(session[:usuario_id])
      if @usuario.cambiar_password(params[:password])
        flash[:notice] = "Password cambiado correctamente"
      else
        flash[:notice] = "Se produjo un error al cambiar el password"
      end
    end
  end
end
