class ProyectosController < ApplicationController
  def listar
    @proyectos = Proyecto.find :all
  end

  def agregar
    @proyecto = Proyecto.new(params[:proyecto])
    if request.post?
      if @proyecto.save
        redirect_to :action => :listar
      end
    end
  end


  def editar
    @proyecto = Proyecto.find params[:id]
    if request.post?
      if @proyecto.update_attributes(params[:proyecto])
        redirect_to :action => :listar
      end
    end
  end

  def eliminar
    @proyecto = Proyecto.find(params[:id])
    @proyecto.destroy
    redirect_to :action => :listar
  end

end
