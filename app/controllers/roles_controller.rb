class RolesController < ApplicationController
  def listar
    @roles = Rol.find(:all)
  end

  def agregar
    @rol = Rol.new(params[:rol])
    if request.post?
      if @rol.save
        flash[:notice] = 'Rol agregado exitosamente'
        redirect_to :action => 'editar', :id => @rol
      end
    end
  end

  def editar
    @rol = Rol.find(params[:id])
    if request.get?
      @derechos_que_tiene = @rol.derechos(:order => 'controller, action').collect { |d| [d.controller.humanize+": "+d.action.humanize, d.id] }
      @derechos_que_no_tiene = (Derecho.find(:all, :order => 'controller, action') - @rol.derechos).collect { |d| [d.controller.humanize+": "+d.action.humanize, d.id] }
    elsif request.post?
      @rol.ganar_derechos(params[:derechos_a_ganar]||[])
      @rol.perder_derechos(params[:derechos_a_perder]||[])
      redirect_to :action => 'editar', :id=>@rol
    end
  end

  def eliminar
    @rol = Rol.find(params[:id])
    if @rol.destroy
      flash[:notice] = "Rol #{@rol.nombre} eliminado"
    else
      flash[:notice] = "No se pudo borrar #{@rol.nombre}"
    end
    redirect_to :action => 'listar'
  end


  private

  def edit
    @arbol_de_roles = {}
    roles = Rol.find(:all)
    roles.each do |r|
      ph = {}
      derechos = r.derechos
      derechos.each do |d|
        ph[d.controller] ||= []
        ph[d.controller] << d.action
      end
      @arbol_de_roles[r.nombre] = ph
    end

    @arbol_de_derechos = {}
    derechos = Derecho.find(:all)
    derechos.each do |d|
      @arbol_de_derechos[d.controller] ||= []
      @arbol_de_derechos[d.controller] << d.action
    end
  end





end
