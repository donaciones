class RespuestasController < ApplicationController

  def listar
    @respuestas_pac = RespuestaPac.find(:all)
    @respuestas_pat = RespuestaPat.find(:all)
  end

  def editar
    @respuesta = Respuesta.find params[:id]
    if request.post?
      if @respuesta.update_attributes(params[:respuesta])
        redirect_to :action => :listar
      end
    end
  end
  
  def agregar_pat
    @respuesta = RespuestaPat.new(params[:respuesta])
    if request.post?
      if @respuesta.save
        redirect_to :action => :listar
      end
    end
  end

  def agregar_pac
    @respuesta = RespuestaPac.new(params[:respuesta])
    if request.post?
      if @respuesta.save
        redirect_to :action => :listar
      end
    end
  end

  def eliminar
    @respuesta = Respuesta.find(params[:id])
    @respuesta.destroy
    redirect_to :action => :listar
  end
end
