# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of ActiveRecord to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 43) do

  create_table "bancos", :force => true do |t|
    t.string "nombre"
    t.string "alineacion_output"
    t.string "codigo"
  end

  create_table "claves_publicas", :force => true do |t|
    t.text "contenido"
  end

  create_table "comunas", :force => true do |t|
    t.string "nombre"
  end

  create_table "derechos", :force => true do |t|
    t.string "nombre"
    t.string "controller"
    t.string "action"
  end

  create_table "derechos_roles", :id => false, :force => true do |t|
    t.integer "derecho_id"
    t.integer "rol_id"
  end

  create_table "donaciones", :force => true do |t|
    t.integer "mandato_id"
    t.integer "proceso_id"
    t.decimal "monto_real",   :precision => 12, :scale => 2
    t.integer "respuesta_id"
  end

  create_table "donaciones_huerfanas", :force => true do |t|
    t.string  "rut"
    t.string  "banco_id"
    t.integer "proceso_id"
  end

  create_table "mandatos", :force => true do |t|
    t.decimal "monto",             :precision => 12, :scale => 2
    t.integer "moneda_id"
    t.date    "fecha_de_creacion"
    t.date    "periodo_inicial"
    t.date    "periodo_final"
    t.integer "medio_de_pago_id"
    t.boolean "indefinido"
    t.boolean "habilitado"
  end

  create_table "mandatos_proyectos", :id => false, :force => true do |t|
    t.integer "mandato_id"
    t.integer "proyecto_id"
  end

  create_table "medios_de_pago", :force => true do |t|
    t.string  "type"
    t.integer "persona_id"
    t.string  "numero"
    t.integer "tipo_id"
    t.date    "fecha_de_vencimiento"
    t.string  "nombre_titular"
    t.integer "banco_id"
    t.text    "numero_encriptado"
    t.string  "ultimos_digitos"
    t.boolean "habilitado"
  end

  create_table "monedas", :force => true do |t|
    t.string  "nombre"
    t.string  "simbolo"
    t.boolean "es_primaria"
  end

  create_table "personas", :force => true do |t|
    t.string  "nombres"
    t.string  "apellido_paterno"
    t.string  "apellido_materno"
    t.string  "rut"
    t.string  "dv"
    t.string  "sexo"
    t.string  "apelativo"
    t.string  "direccion1"
    t.string  "direccion2"
    t.string  "telefono1"
    t.string  "telefono2"
    t.string  "email"
    t.integer "comuna_id"
  end

  create_table "procesos", :force => true do |t|
    t.date   "periodo"
    t.date   "fecha"
    t.string "tipo"
    t.string "estado"
  end

  create_table "proyectos", :force => true do |t|
    t.string "nombre"
  end

  create_table "respuestas", :force => true do |t|
    t.string  "type"
    t.string  "codigo"
    t.string  "descripcion"
    t.boolean "efectuada"
  end

  create_table "respuestas_pac", :force => true do |t|
    t.string "codigo"
    t.string "descripcion"
  end

  create_table "roles", :force => true do |t|
    t.string "nombre"
  end

  create_table "roles_usuarios", :id => false, :force => true do |t|
    t.integer "rol_id"
    t.integer "usuario_id"
  end

  create_table "tipos", :force => true do |t|
    t.string "nombre"
    t.string "mascara"
  end

  create_table "tipos_de_cambio", :force => true do |t|
    t.integer "moneda_id"
    t.float   "valor"
    t.date    "fecha"
  end

  create_table "usuarios", :force => true do |t|
    t.string "nombre"
    t.string "password_encriptado"
  end

end
