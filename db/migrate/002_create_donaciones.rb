class CreateDonaciones < ActiveRecord::Migration
  def self.up
    create_table :donaciones do |t|
      t.column :mandato_id,       :integer
      t.column :proceso_id,       :integer
      t.column :monto_real,       :integer
      t.column :respuesta_banco,  :string
    end
  end

  def self.down
    drop_table :donaciones
  end
end
