class AddEstadoToProcesos < ActiveRecord::Migration
  def self.up
    add_column :procesos, :estado, :string
  end

  def self.down
    drop_column :procesos, :estado
  end
end
