class AddHabilitadoToMedioDePago < ActiveRecord::Migration
  def self.up
    add_column :medios_de_pago, :habilitado, :boolean
  end

  def self.down
    remove_column :medios_de_pago, :habilitado
  end
end
