class CreateMediosDePago < ActiveRecord::Migration
  def self.up
    create_table :medios_de_pago do |t|
      t.column :type,                 :string

      #atributos comunes
      t.column :persona_id,           :integer
      t.column :numero,               :string

      #atributos tarjeta_de_credito
      t.column :tipo_id,              :integer
      t.column :fecha_de_vencimiento, :date
      t.column :nombre_titular,       :string

      #atributos cuenta_corriente
      t.column :banco_id,             :integer
    end
  end

  def self.down
    drop_table :medios_de_pago
  end
end
