class CreateAdminUser < ActiveRecord::Migration
  def self.up
    usuario_admin = Usuario.create(:nombre => 'admin', :password => 'admin')
    rol_admin = Rol.create(:nombre => 'Administrador')
    rol_admin.derechos << Derecho.find(:all)
    usuario_admin.roles << rol_admin
  end

  def self.down
    if u = Usuario.find_by_nombre("admin")
      u.destroy
    end
    if r = Rol.find_by_nombre("Administrador")
      r.destroy
    end
  end
end
