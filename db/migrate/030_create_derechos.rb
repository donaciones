class CreateDerechos < ActiveRecord::Migration
  def self.up
    create_table :derechos do |t|
      t.column :nombre, :string
      t.column :controller, :string
      t.column :action, :string
    end
  end

  def self.down
    drop_table :derechos
  end
end
