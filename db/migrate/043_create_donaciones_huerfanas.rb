class CreateDonacionesHuerfanas < ActiveRecord::Migration
  def self.up
    create_table :donaciones_huerfanas do |t|
      t.string 'rut'
      t.string 'banco_id'
      t.integer 'proceso_id'
    end
  end

  def self.down
    drop_table :donaciones_huerfanas
  end
end
