class CreatePersonas < ActiveRecord::Migration
  def self.up
    create_table :personas do |t|
      t.column :nombres,          :string
      t.column :apellido_paterno, :string
      t.column :apellido_materno, :string
      t.column :rut,              :string
      t.column :dv,               :string
      t.column :sexo,             :string
      t.column :apelativo,        :string
      t.column :direccion1,       :string
      t.column :direccion2,       :string
      t.column :telefono1,        :string
      t.column :telefono2,        :string
      t.column :email,            :string
      t.column :comuna_id,        :integer
    end
  end

  def self.down
    drop_table :personas
  end
end
