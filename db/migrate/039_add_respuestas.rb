class AddRespuestas < ActiveRecord::Migration
  def self.up
    # PAT
    RespuestaPat.new(:codigo => "0", :descripcion => "Aprobada").save
    RespuestaPat.new(:codigo => "3", :descripcion => "Rechazo").save
    RespuestaPat.new(:codigo => "16", :descripcion => "Rechazo definitivo").save
    
    #PAC
    RespuestaPac.new(:codigo => "0", :descripcion => "Registro rechazado en proceso de validación").save
    RespuestaPac.new(:codigo => "1", :descripcion => "Cargo no efecutado por orden de no cargo al servicio").save
    RespuestaPac.new(:codigo => "2", :descripcion => "Cargo no efecutado por exceder monto límite").save
    RespuestaPac.new(:codigo => "3", :descripcion => "Cargo efecutado en cuenta corriente").save
    RespuestaPac.new(:codigo => "5", :descripcion => "Cargo no efecutado por no existir cuenta corriente").save
    RespuestaPac.new(:codigo => "6", :descripcion => "Cargo no efecutado por cuenta corriente cerrada").save
    RespuestaPac.new(:codigo => "7", :descripcion => "Cargo no efecutado por cuenta corriente bloqueada").save
    RespuestaPac.new(:codigo => "8", :descripcion => "Cargo no efectuado por cuenta corriente sin fondos").save
    RespuestaPac.new(:codigo => "A", :descripcion => "Cargo no efecutado por servicio eliminado").save
  end

  def self.down
    Respuesta.delete_all
  end

end

