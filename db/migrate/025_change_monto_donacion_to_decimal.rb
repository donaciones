class ChangeMontoDonacionToDecimal < ActiveRecord::Migration
  def self.up
    change_column :donaciones, :monto_real,:decimal, :precision => 12, :scale => 2
  end

  def self.down
    change_column :donaciones, :monto_real, :integer
  end
end
