class CreateRespuestas < ActiveRecord::Migration
  def self.up
    create_table :respuestas do |t|
      t.column :type, :string

      # Atributos comunes
      t.column :codigo, :string
      t.column :descripcion, :string
    end
  end

  def self.down
    drop_table :respuestas
  end
end
