class CreateTiposDeCambio < ActiveRecord::Migration
  def self.up
    create_table :tipos_de_cambio do |t|
      t.column :moneda_id,  :integer
      t.column :valor,      :float
      t.column :fecha,      :date
    end
  end

  def self.down
    drop_table :tipos_de_cambio
  end
end
