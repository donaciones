class CreateComunas < ActiveRecord::Migration
  def self.up
    create_table :comunas do |t|
      t.column :nombre,   :string
    end
  end

  def self.down
    drop_table :comunas
  end
end
