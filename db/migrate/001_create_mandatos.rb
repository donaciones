class CreateMandatos < ActiveRecord::Migration
  def self.up
    create_table :mandatos do |t|
      t.column :monto,              :integer
      t.column :moneda_id,          :integer
      t.column :fecha_de_creacion,  :date
      t.column :periodo_inicial,    :date
      t.column :periodo_final,      :date
      t.column :medio_de_pago_id,   :integer
    end
  end

  def self.down
    drop_table :mandatos
  end
end
