class AddHabilitadoToMandato < ActiveRecord::Migration
  def self.up
    add_column :mandatos, :habilitado, :boolean
  end

  def self.down
    remove_column :mandatos, :habilitado
  end
end
