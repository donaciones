class CreateDerechosRoles < ActiveRecord::Migration
  def self.up
    create_table :derechos_roles, :id => false do |t|
      t.column :derecho_id, :integer
      t.column :rol_id, :integer
    end
  end

  def self.down
    drop_table :derechos_roles
  end
end
