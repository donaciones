class AddUltimosDigitosToTarjeta < ActiveRecord::Migration
  def self.up
    add_column :medios_de_pago, :ultimos_digitos, :string
  end

  def self.down
    remove_column :medios_de_pago, :ultimos_digitos
  end
end
