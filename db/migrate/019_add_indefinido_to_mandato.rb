class AddIndefinidoToMandato < ActiveRecord::Migration
  def self.up
    add_column :mandatos, :indefinido, :boolean
  end

  def self.down
    remove_column :mandatos, :indefinido
  end
end
