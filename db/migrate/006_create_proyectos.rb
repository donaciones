class CreateProyectos < ActiveRecord::Migration
  def self.up
    create_table :proyectos do |t|
      t.column :nombre, :string
    end
  end

  def self.down
    drop_table :proyectos
  end
end
