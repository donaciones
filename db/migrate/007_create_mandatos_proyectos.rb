class CreateMandatosProyectos < ActiveRecord::Migration
  def self.up
    create_table :mandatos_proyectos, :id=>false do |t|
      t.column :mandato_id,   :integer
      t.column :proyecto_id,  :integer
    end
  end

  def self.down
    drop_table :mandatos_proyectos
  end
end
