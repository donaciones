class AddEsprimariaToMonedas < ActiveRecord::Migration
  def self.up
    add_column :monedas, :es_primaria, :boolean
  end

  def self.down
    remove_column :monedas, :es_primaria
  end
end
