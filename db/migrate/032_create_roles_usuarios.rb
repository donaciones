class CreateRolesUsuarios < ActiveRecord::Migration
  def self.up
    create_table :roles_usuarios, :id => false do |t|
      t.column :rol_id, :integer
      t.column :usuario_id, :integer
    end
  end

  def self.down
    drop_table :roles_usuarios
  end
end
