class CreateMonedas < ActiveRecord::Migration
  def self.up
    create_table :monedas do |t|
      t.column :nombre, :string
      t.column :simbolo, :string
    end
  end

  def self.down
    drop_table :monedas
  end
end
