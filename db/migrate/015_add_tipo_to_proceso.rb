class AddTipoToProceso < ActiveRecord::Migration
  def self.up
    add_column :procesos, :tipo, :string
  end

  def self.down
    remove_column :procesos, :tipo
  end
end
