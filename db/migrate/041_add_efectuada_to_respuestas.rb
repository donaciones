class AddEfectuadaToRespuestas < ActiveRecord::Migration
  def self.up
    add_column :respuestas, :efectuada, :boolean
  end

  def self.down
    remove_column :respuestas, :efectuada
  end
end
