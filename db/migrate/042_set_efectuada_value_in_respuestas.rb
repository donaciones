class SetEfectuadaValueInRespuestas < ActiveRecord::Migration
  def self.up
    respuestas = Respuesta.find(:all)
    respuestas.each do |r|
      r.efectuada = false
      r.save
    end

    respuestas_ok = RespuestaPat.find_by_codigo(0), RespuestaPac.find_by_codigo(3)
    respuestas_ok.each do |r|
      r.efectuada = true
      r.save
    end

  end

  def self.down
  end
end
