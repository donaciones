class ChangeMontoToDecimal < ActiveRecord::Migration
  def self.up
    change_column :mandatos, :monto, :decimal, :precision => 12, :scale => 2
  end

  def self.down
    change_column :mandatos, :monto, :integer
  end
end
