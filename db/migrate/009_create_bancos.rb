class CreateBancos < ActiveRecord::Migration
  def self.up
    create_table :bancos do |t|
      t.column :nombre,           :string
      t.column :alineacion_output,  :string
      t.column :codigo,           :string
    end
  end

  def self.down
    drop_table :bancos
  end
end
