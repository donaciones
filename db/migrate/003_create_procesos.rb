class CreateProcesos < ActiveRecord::Migration
  def self.up
    create_table :procesos do |t|
      t.column :periodo, :date
    end
  end

  def self.down
    drop_table :procesos
  end
end
