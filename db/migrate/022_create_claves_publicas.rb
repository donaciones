class CreateClavesPublicas < ActiveRecord::Migration
  def self.up
    create_table :claves_publicas do |t|
      t.column :contenido, :text
    end

    ClavePublica.create({:contenido=>""})
  end

  def self.down
    drop_table :claves_publicas
  end
end
