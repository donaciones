class CreateTipos < ActiveRecord::Migration
  def self.up
    create_table :tipos do |t|
      t.column :nombre,   :string
      t.column :mascara,  :string
    end
  end

  def self.down
    drop_table :tipos
  end
end
