class CreateUsuarios < ActiveRecord::Migration
  def self.up
    create_table :usuarios do |t|
      t.column :nombre, :string
      t.column :password_encriptado, :string
    end
  end

  def self.down
    drop_table :usuarios
  end
end
