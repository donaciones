class AddNumeroTarjetaEncriptado < ActiveRecord::Migration
  def self.up
    add_column :medios_de_pago, :numero_encriptado, :text
  end

  def self.down
    remove_column :medios_de_pago, :numero_encriptado
  end
end
