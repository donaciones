class AddFechaToProceso < ActiveRecord::Migration
  def self.up
    add_column :procesos, :fecha, :date
  end

  def self.down
    remove_column :procesos, :fecha
  end
end
