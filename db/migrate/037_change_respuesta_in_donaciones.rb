class ChangeRespuestaInDonaciones < ActiveRecord::Migration
  def self.up
    add_column :donaciones, :respuesta_id, :integer
    remove_column :donaciones, :respuesta_banco
  end

  def self.down
    add_column :donaciones, :respuesta_banco, :string
    remove_column :donaciones, :respuesta_id
  end
end
