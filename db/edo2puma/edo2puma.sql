-- bancos
select row_id, banco, 'izquierda', cod_banco from bancos into outfile 'bancos.csv';

-- comunas
select comuna,nomcomuna from comunas into outfile 'comunas.csv';

-- mandatos (de ctascte)
select row_id, aporte, tipo, concat(agnoini,'-',mesini,'-01'), concat(agnoini, '-', mesini, '-01'), NULL, row_id, 't' from ctascte where vigente=1 into outfile 'mandatos__ctascte.csv';

-- mandatos (de tarjetas)
select row_id+1000, aporte, tipo, concat(agnoini,'-',mesini,'-01'), concat(agnoini,'-',mesini,'-01'),NULL, row_id+1000, 't' from tarjetas where vigente=1 into outfile 'mandatos__tarjetas.csv';

-- mandatos_proyectos (de ctascte)
select row_id, proyecto from ctascte where vigente=1 into outfile 'mandatos_proyectos__ctascte.csv';

-- mandatos_proyectos (de tarjetas)
select row_id+1000, proyecto from tarjetas where vigente=1 into outfile 'mandatos_proyectos__tarjetas.csv';

-- medios de pago (de ctascte)
select ctascte.row_id, "CuentaCorriente", colabor.row_id, ctacte, NULL, NULL, NULL, bancos.row_id, NULL, NULL, 't' from ctascte, colabor, bancos where ctascte.rut = colabor.rut and ctascte.banco = bancos.cod_banco and vigente=1 into outfile 'medios_de_pago__ctascte.csv';

-- medios_de_pago (de tarjetas)
select tarjetas.row_id+1000, "TarjetaDeCredito", colabor.row_id, numtar, tipotar, concat(agnovenc,'-',mesvenc, '-01'), NULL, NULL, NULL, NULL, 't' from tarjetas, colabor where tarjetas.rut = colabor.rut and tarjetas.numtar !='' and vigente=1 into outfile 'medios_de_pago__tarjetas.csv';

-- personas
select colabor.row_id, nombres, apellidop, apellidom, concat(colabor.rut, '-', dv), ' ',if(sexo=0,'masculino','femenino'), apelat, direccion, ' ', fono1, fono2, email, comuna from colabor, personal where colabor.rut = personal.rut into outfile 'personas.csv';





