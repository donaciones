INSERT INTO bancos VALUES(1,'Banco De Chile','izquierda','001');
INSERT INTO bancos VALUES(2,'Banco Scotiabank (Sudamericano)','izquierda','014');
INSERT INTO bancos VALUES(3,'Banco Credito','derecha','016');
INSERT INTO bancos VALUES(4,'Banco De A. Edwards','izquierda','029');
INSERT INTO bancos VALUES(5,'Banco Santiago','izquierda','035');
INSERT INTO bancos VALUES(6,'Banco Santander','derecha','037');
INSERT INTO bancos VALUES(7,'Bank Boston','izquierda','039');
INSERT INTO bancos VALUES(8,'Banco Del Desarrollo','izquierda','507');
INSERT INTO bancos VALUES(9,'Banco Citibank','izquierda','033');
INSERT INTO bancos VALUES(10,'Banco Bice','izquierda','028');
INSERT INTO bancos VALUES(11,'Banco Security','izquierda','049');
INSERT INTO bancos VALUES(12,'Bbva Chile','izquierda','504');
INSERT INTO bancos VALUES(13,'Banco Corpbanca','izquierda','027');

INSERT INTO tipos VALUES(1,'Visa','XXXX-XXXX-XXXX-XXXX');
INSERT INTO tipos VALUES(2,'MasterCard','XXXX-XXXX-XXXX-XXXX');
INSERT INTO tipos VALUES(3,'Diners','XXXX-XXXXXX-XXXX');
INSERT INTO tipos VALUES(4,'Magna','XXXX-XXXX-XXXX-XXXX');
INSERT INTO tipos VALUES(5,'American Express','XXXX-XXXX-XXXX-XXX');

INSERT INTO monedas VALUES(1,'UF','UF','f');
INSERT INTO monedas VALUES(2,'Peso','$','t');
INSERT INTO monedas VALUES(3,'Dólar','US$','f');

INSERT INTO comunas VALUES(1,'Providencia');
INSERT INTO comunas VALUES(2,'Ñuñoa');
INSERT INTO comunas VALUES(3,'Las Condes');
INSERT INTO comunas VALUES(4,'Lo Barnechea');
INSERT INTO comunas VALUES(5,'Vitacura');
INSERT INTO comunas VALUES(6,'Macul');
INSERT INTO comunas VALUES(7,'La Reina');
INSERT INTO comunas VALUES(8,'Santiago');
INSERT INTO comunas VALUES(9,'Estación Central');
INSERT INTO comunas VALUES(10,'Huechuraba');
INSERT INTO comunas VALUES(11,'La Florida');
INSERT INTO comunas VALUES(12,'Peñalolén');
INSERT INTO comunas VALUES(13,'Pedro Aguirre Cerda');
INSERT INTO comunas VALUES(14,'Maipú');
INSERT INTO comunas VALUES(15,'La Dehesa');
INSERT INTO comunas VALUES(16,'Pirque');
INSERT INTO comunas VALUES(17,'Colina');
INSERT INTO comunas VALUES(18,'Concepción');
INSERT INTO comunas VALUES(19,'San Antonio');
INSERT INTO comunas VALUES(20,'Talagante');
INSERT INTO comunas VALUES(21,'Punta Arenas');
INSERT INTO comunas VALUES(22,'San Fernando');
INSERT INTO comunas VALUES(23,'San Joaquín');
INSERT INTO comunas VALUES(24,'Antofagasta');
INSERT INTO comunas VALUES(25,'Buin');
INSERT INTO comunas VALUES(26,'San Miguel');

INSERT INTO proyectos VALUES(1,'CVX Y Exalumnos');
INSERT INTO proyectos VALUES(2,'CAF');
INSERT INTO proyectos VALUES(3,'Proyecto Puente');
INSERT INTO proyectos VALUES(4,'Ignacianos 65');
INSERT INTO proyectos VALUES(5,'SIAO 1973');

