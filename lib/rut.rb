class RUT
  def self.formatear(r)
    return nil if self.caracteres_invalidos(r)
    r.delete!(".-")
    numero = r[0..-2]
    dv = r[-1,1]
    numero.to_s.gsub(/(\d)(?=(\d\d\d)+(?!\d))/, "\\1.")+"-"+dv
  end

  def self.formatear_sin_puntos(r)
    self.formatear(r).delete(".")
  end

  def self.reducir(r)
    return nil if self.caracteres_invalidos(r)
    r.delete(".-")
  end

  def self.reducir_sin_dv(r)
    return nil if self.caracteres_invalidos(r)
    r.gsub(/-.$/,'')
  end


  def self.valido?(rut)
    rut = self.formatear_sin_puntos(rut)

    rut_test, dv = rut.to_s.upcase.gsub(/[^0-9K-]/,'').split("-")
    rut_test = rut_test.to_i
    dv = dv.to_s

    v=1
    s=0
    for i in (2..9)
      if i == 8
        v=2
      else
        v+=1
      end
      s+=v*(rut_test%10)
      rut_test/=10
    end

    s = 11 - s%11

    if s == 11
      correct_dv = "0"
    elsif s == 10
      correct_dv = "K"
    else
      correct_dv = s.to_s
    end

    dv == correct_dv
  end

  private

  def self.caracteres_invalidos(r)
    r[/[^-0-9.Kk]/]
  end

end
