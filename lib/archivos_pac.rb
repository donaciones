require 'rut'
require 'iconv'

class ArchivosPAC

  def self.leer_bdu(archivo)
    lineas = archivo.read.scan(/^.*$/)
    mandatos = []


    lineas.each do |linea|
      if linea[9,1] == 'D'
        codigo_banco = linea[0..2]
        rut = linea[10..31].delete(' ').gsub(/^0*/,'')
        mandatos << [rut, codigo_banco]
      end
    end

    # TODO: comprobación de integridad del archivo

    mandatos
  end

  def self.generar_cargos(fecha, mandatos)
    texto_archivo = ""
    monto_total = 0

    fecha=fecha.to_s.delete("-")

    mandatos.each do |mandato|
      nombre = "#{mandato[:apellido_paterno]} #{mandato[:apellido_materno]} #{mandato[:nombres]}".chars.upcase[0,10]
      monto = "%011d"%(mandato[:monto]*100)
      monto_total += mandato[:monto]
      rut = RUT.reducir(mandato[:rut])
      if mandato[:alineacion] == "izquierda"
        rut = "0"*(9-rut.size)+rut+" "*13
      else
        rut = "0"*(22-rut.size)+rut
      end

      texto_archivo << "#{mandato[:banco]}596001D#{rut} #{nombre}#{monto}#{fecha}#{fecha}..........\n"
    end

    cantidad_registros = "%06d"%mandatos.size
    monto_total = "%09d00"%monto_total

    # reg. control
    texto_archivo << "001596001T"+" "*33+"#{monto_total}#{cantidad_registros}"+"."*20+"\n"
    texto_archivo = Iconv.conv("ISO-8859-1", "UTF-8", texto_archivo)
  end


  def self.leer_cargos(archivo) #TODO
    lineas = archivo.read.scan(/^.*$/)
    cargos = []

    lineas.each do |linea|
      if linea[9,1] == 'D'
        codigo_banco = linea[0..2]
        rut = linea[10..31].delete(' ').gsub(/^0*/,'')
        monto = linea[33..41].to_i
        codigo_respuesta = linea[60..60]

        cargos << {
          :rut => rut,
          :monto => monto,
          :codigo_banco => codigo_banco,
          :codigo_respuesta => codigo_respuesta,
          :respuesta => respuesta(codigo_respuesta)
        }
        
      end
    end

    cargos
  end

  def self.respuesta(codigo)
    case codigo
    when '3'
      'cargo efectuado'
    when '1', '2', '5', '6', '7', '8', 'A'
      'cargo no efectuado'
    when '0'
      'registro rechazado'
    end
  end
    
end
