require 'rut'

class ArchivosPAT

  def self.generar_cargos(fecha, mandatos)
    texto_archivo = ""
    monto_total = 0

    fecha=fecha.to_s.delete("-")

    mandatos.each do |mandato|
      nombre = "#{mandato[:apellido_paterno]} #{mandato[:apellido_materno]} #{mandato[:nombres]}".chars.upcase[0,10]
      monto = "%011d"%(mandato[:monto]*100)
      monto_total += mandato[:monto]
      rut = RUT.reducir(mandato[:rut])
      if mandato[:alineacion] == "izquierda"
        rut = "0"*(9-rut.size)+rut+" "*13
      else
        rut = "0"*(22-rut.size)+rut
      end

      texto_archivo << "#{mandato[:banco]}596001D#{rut} #{nombre}#{monto}#{fecha}#{fecha}..........\n"
    end

    cantidad_registros = "%06d"%mandatos.size
    monto_total = "%09d00"%monto_total

    # reg. control
    texto_archivo << "001596001T"+" "*33+"#{monto_total}#{cantidad_registros}"+"."*20+"\n"
  end


  def self.leer_cargos(archivo) #TODO
    lineas = archivo.read.scan(/^.*$/)
    cargos = []

    lineas.each do |linea|
      linea=linea.split(";")
      numero_tarjeta = linea[2]
      monto = linea[1].to_i
      fecha_expiracion = linea[3]
      codigo_respuesta = linea[12]
      respuesta = linea[13]
      rut = linea[8]

      cargos << {
        :rut => rut,
        :numero_tarjeta => numero_tarjeta,
        :fecha_expiracion => fecha_expiracion,
        :monto => monto,
        :codigo_respuesta => codigo_respuesta,
        :respuesta => respuesta
      }
    end

    # TODO: comprobación de integridad del archivo

    cargos
  end
end
