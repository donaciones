set :application, "donaciones"
set :repository, "git://github.com/jipumarino/donaciones.git"
#set :branch, 'master'


# If you aren't deploying to /u/apps/#{application} on the target
# servers (which is the default), you can specify the actual location
# via the :deploy_to variable:
set :deploy_to, "/home/apache/#{application}" # defaults to "/u/apps/#{application}"

set :deploy_via, :remote_cache

set :user, "jipumarino"            # defaults to the currently logged in user
set :mongrel_conf, "#{current_path}/config/mongrel_cluster.yml"

set :branch, "pre-1.0"

# If you aren't using Subversion to manage your source code, specify
# your SCM below:
set :scm, :git

role :web, "colborja"
role :app, "colborja"
role :db,  "colborja", :primary => true
